const glGot = require('gl-got')

const defaultSignaturesUrl = 'https://gitlab.com/api/v4/projects/10416318/repository/files/signatures.json/raw?ref=master'
const getSignatures = (url = defaultSignaturesUrl) => {
  return glGot.get(url)
    .then(signatures => signatures)
    .catch(err => err)
}

module.exports = getSignatures
