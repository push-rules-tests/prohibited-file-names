const prohibitedFileNames = require('.')

class CliController {
  constructor(destinationDir) {
    this.destinationDir = destinationDir
  }

  async recreate(destDir = this.destinationDir) {
    console.log('[prohibited-file-names] Fixture Factory')
    console.log(`  Cleaning all directories and files in "${destDir}"...`)
    try {
      await prohibitedFileNames.fixtures.clean(destDir)
      console.log('  ...done.')
    } catch (err) {
      console.error(err)
    }
    console.log('  Creating new test fixtures for data-leakage detection...')
    try {
      const count = await prohibitedFileNames.fixtures.create(destDir)
      console.log('  ...done. Created %i directories and files in "%s".', count, destDir)
      console.log('\n  Now go plug those leaks. Happy testing!\n')
    } catch (err) {
      console.error(err)
    }
  }
}

module.exports = CliController
