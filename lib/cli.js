#!/usr/bin/env node
'use strict';
const meow = require('meow');
const CliController = require('./cli-controller');

const options = {
  flags: {
    dest: {
      type: 'string',
      default: './lib/__tests__/__fixtures__'
    }
  }
}

const helpMsg = `
Usage
  $ prohibited-file-names [options]

Options
  --dest  Destination directory.
          [Default: ./lib/__tests__/__fixtures__]

Examples
  $ prohibited-file-names
  Created 91 data-leakage detection test fixtures.
`
const cli = meow(helpMsg, options)

const main = () => {
  const ctrl = new CliController(cli.flags.dest)
  ctrl.recreate()
}

main()
