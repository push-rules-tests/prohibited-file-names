const RandExp = require('randexp')
const getSignatures = require('./get-signatures')
const removeImpermissibleChars = require('./remove-impermissible-chars')

const prohibitedFileNameFactory = {
  async create() {
    let signatures = []
    try {
      const { body } = await this.getSignatures()
      signatures = body
        .filter((signature) => signature.part !== 'contents')
        .map((signature) => {
          const fileName = new RandExp(signature.pattern).gen()
          signature.fileName = removeImpermissibleChars(fileName)
          return signature
        })
    } catch (err) {
      throw err
    }
    return signatures
  },

  getSignatures
}

module.exports = prohibitedFileNameFactory
