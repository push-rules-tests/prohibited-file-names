const fsExtra = require('fs-extra')
const prohibitedFileNameFactory = require('./prohibited-file-name-factory')
const destDir = './lib/__tests__/__fixtures__'

const writeFileFixture = async (signature, dest = destDir) => {
  const fileName = `${dest}/${signature.fileName}`
  try {
    await fsExtra.outputFile(fileName, '')
    return true
  } catch (err) {
    return false
  }
}

const writeDirectoryFixture = async (signature, dest = destDir) => {
  const directoryPath = `${dest}/${signature.fileName}`
  try {
    await fsExtra.emptyDir(directoryPath)
    return true
  } catch (err) {
    return false
  }
}

const fixtureMap = new Map([
  ['extension', (signature, dest) => writeFileFixture(signature, dest)],
  ['filename', (signature, dest) => writeFileFixture(signature, dest)],
  ['path', (signature, dest) => writeDirectoryFixture(signature, dest)]
])

const createFixtures = async (destinationDir = destDir) => {
  let signatures = []
  let count = 0
  try {
    signatures = await prohibitedFileNameFactory.create()
    signatures.forEach((signature) => {
      fixtureMap.get(signature.part)(signature, destinationDir)
    })
    count = signatures.length
  } catch (err) {
    count = 0
  }
  return count
}

module.exports = createFixtures
