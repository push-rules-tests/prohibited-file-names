const fsExtra = require('fs-extra')

const cleanFixtures = async (fixturesDir) => {
  let success = true
  try {
    await fsExtra.remove(fixturesDir)
  } catch (err) {
    success = false
  }
  return success
}

module.exports = cleanFixtures
