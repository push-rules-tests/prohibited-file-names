const cleanFixtures = require('./clean-fixtures')
const createFixtures = require('./create-fixtures')

const fixtureFactory = {
  'clean': cleanFixtures,
  'create': createFixtures,
  'destinationDirectory': './lib/__tests__/__fixtures__',
  async recreate(dest = this.destinationDirectory) {
    let success = true
    try {
      await cleanFixtures(dest)
      await createFixtures(dest)
    } catch (err) {
      success = false
    }
    return success
  }
}

module.exports = fixtureFactory
