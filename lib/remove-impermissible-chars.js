const excludedChars = [
  /[^A-Za-z0-9_\.\/]/gm
]

const removeImpermissibleChars = (fileName) => {
  let name = fileName
  excludedChars.forEach((regex) => {
    name = name.replace(regex, '')
  })
  return name
}

module.exports = removeImpermissibleChars
