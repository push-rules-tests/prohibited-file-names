const fixtureFactory = require('./fixture-factory')

const prohibitedFileNames = {
  'fixtures': fixtureFactory
}

module.exports = prohibitedFileNames
