# prohibited-file-names 

[![NPM version][npm-image]][npm-url]
[![Build Status][travis-image]][travis-url]
[![Dependency Status][daviddm-image]][daviddm-url]
[![Coverage percentage][coveralls-image]][coveralls-url]

> Unit and functional specifications that test GitLab Push Rules Prohibited file names.

## Installation

```bash
npm install --save prohibited-file-names
```

## Usage

Automatically generate test fixtures for

- Directories
- File names
- File paths

These file system fixtures all match data-leakage signatures that _should_ be
detected with pre-commit or pre-receive hooks.

From a terminal, enter:

```bash
prohibited-file-names
# => Creates directories and files in the './lib/__tests__/__fixtures__
#    directory by default.
```

You can also specify a destination directory for fixtures:

```bash
prohibited-file-names --dest="~/Projects/security/data-leakage-fixtures"
```

## License

MIT © [Greg Swindle](https://gitlab.com/gregswindle)


[npm-image]: https://badge.fury.io/js/prohibited-file-names.svg
[npm-url]: https://npmjs.org/package/prohibited-file-names
[travis-image]: https://travis-ci.org/push-rule-tests/prohibited-file-names.svg?branch=master
[travis-url]: https://travis-ci.org/push-rule-tests/prohibited-file-names
[daviddm-image]: https://david-dm.org/push-rule-tests/prohibited-file-names.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/push-rule-tests/prohibited-file-names
[coveralls-image]: https://coveralls.io/repos/push-rule-tests/prohibited-file-names/badge.svg
[coveralls-url]: https://coveralls.io/r/push-rule-tests/prohibited-file-names
